# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .party import Party, PartyAccount


def register():
    Pool.register(
        Party,
        PartyAccount,
        module='party_subsidy', type_='model')
