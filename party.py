# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from simpleeval import simple_eval
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.tools import decistmt
from trytond.i18n import gettext
from trytond.exceptions import UserError


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    subsidy_formula = fields.Char('Subsidy formula',
        help=('Python expression that will be evaluated with:\n'
            '- untaxed_amount: untaxed amount\n'
            '- total_amount: total amount\n'
            '- quantity: quantity\n'))
    account_subsidy = fields.MultiValue(
        fields.Many2One('account.account', 'Account Subsidy',
        domain=[('kind', '=', 'other'),
                ('company', '=', Eval('context', {}).get('company', -1))])
    )

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'account_subsidy':
            return pool.get('party.party.account')
        return super(Party, cls).multivalue_model(field)

    def check_subsidy_formula(self):
        context = self.get_context_subsidy_formula(field_values={})

        try:
            value = self.get_subsidy_amount(**context)
            if not isinstance(value, Decimal) and not isinstance(
                    Decimal(value), Decimal):
                raise ValueError
        except ValueError as exception:
            raise UserError(gettext(
                'party_subsidy.msg_invalid_subsidy_formula',
                formula=self.subsidy_formula,
                party=self.rec_name,
                exception=exception))

    def get_context_subsidy_formula(self, **field_values):
        return {'names': {
                'untaxed_amount': field_values.get(
                    'untaxed_amount', Decimal('0.0')),
                'total_amount': field_values.get(
                    'total_amount', Decimal('0.0')),
                'quantity': Decimal(field_values.get('quantity', '0.0'))
                },
        }

    def get_subsidy_amount(self, **field_values):
        """Return subsidy amount (as Decimal)"""
        if not self.subsidy_formula:
            return Decimal('0.0')
        context = self.get_context_subsidy_formula(**field_values)
        context.setdefault('functions', {})['Decimal'] = Decimal
        return simple_eval(decistmt(self.subsidy_formula), **context)

    @classmethod
    def validate(cls, records):
        super(Party, cls).validate(records)
        for record in records:
            record.check_subsidy_formula()


class PartyAccount(metaclass=PoolMeta):
    __name__ = 'party.party.account'

    account_subsidy = fields.Many2One('account.account', 'Account Subsidy',
        domain=[('kind', '=', 'other'),
                ('company', '=', Eval('context', {}).get('company', -1))])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)
        if exist:
            exist &= table_h.column_exist('account_subsidy')

        super(PartyAccount, cls).__register__(module_name)

        if not exist:
            # Re-migration
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('account_subsidy')
        value_names.append('account_subsidy')
        super(PartyAccount, cls)._migrate_property(field_names,
            value_names, fields)
